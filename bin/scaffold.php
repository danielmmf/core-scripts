<?php

class Scaffold {

    public static function execute() {
        $shortopts = "";
        $shortopts .= "m:";  // Required value

        $longopts = array(
            "model:", // Required value
        );
        $options = getopt($shortopts, $longopts);
        var_dump($options);
    }
    
    public function call_scaffold_script(){
        print spiffy_model::scaffold( "knowledges" ) ;
    }

}

Scaffold::execute();
?>
