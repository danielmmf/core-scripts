class knowledge extends spiffy_model {

	/**
	 * Configura��o da tabela: campos, tipos e propriedades.
	 *
	 * @return	array	Configura��o do objeto.
	 * @author	?
	 * @since	?
	 */
	protected function get_object_config() {
		global $yes_no_list ;
		return array(
			"table" => "knowledges" ,
			"primary_key" => "knowledge_id" ,
			"names" => array(
				"en" => "knowledge" ,
				"en_plural" => "knowledges" ,
				"pt" => "singular @todo Coloque aqui o nome da entidade no singular (em portugu�s e em min�sculas)" ,
				"pt_plural" => "plural @todo Coloque aqui o nome da entidade no plural (em portugu�s e em min�sculas)" ,
			) ,
			"fields" => array(
				"name" => array(
					"label" => "name" , // @todo Preencha um r�tulo em portugu�s para este campo
					"type" => "text" ,
					"required" => "yes" ,
					"max_length" => 50 ,
				) ,
				"description" => array(
					"label" => "description" , // @todo Preencha um r�tulo em portugu�s para este campo
					"type" => "text" ,
					"required" => "yes" ,
					"max_length" => 65535 ,
				) ,
				"type" => array(
					"label" => "type" , // @todo Preencha um r�tulo em portugu�s para este campo
					"type" => "text" ,
					"required" => "yes" ,
					"max_length" => 50 ,
				) ,
				"visible" => array(
					"label" => "visible" , // @todo Preencha um r�tulo em portugu�s para este campo
					"type" => "yes_no" ,
					"required" => "yes" ,
					"default" => "yes" ,
					"valid" => $yes_no_list ,
				) ,
				"created_by" => array(
					"label" => "Criado por" ,
					"type" => "integer" ,
					"populate" => "no" ,
				) ,
				"modified_by" => array(
					"label" => "Modificado por" ,
					"type" => "integer" ,
					"populate" => "no" ,
				) ,
				"removed_by" => array(
					"label" => "Removido por" ,
					"type" => "integer" ,
					"populate" => "no" ,
				) ,
			) ,
		) ;
	}
}