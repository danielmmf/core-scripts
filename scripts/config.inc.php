<?

/**
 * Testes unitários para classes comuns.
 *
 * @package		spiffy-framework
 * @subpackage	tests
 * @author		rthiago
 * @since		2013-02-04 18:33
 */

setlocale( LC_ALL , "pt_BR.iso88591" ) ;
setlocale( LC_NUMERIC , "C" ) ;

if ( $context == "dev" ) {
	define( "COMMON_DIRECTORY" , "../common/" ) ;
	$debug = true ;

	$db_username = "developers" ;
	$db_password = "gAt01at390" ;
	$db_database = "dev_test_dm" ;
	$db_host = "10.20.26.29" ;

	$checkpoint_service = "dev" ;
	$checkpoint_application_name = "dev" ;
	$checkpoint_application_password = "dev" ;
	$checkpoint_context = "dev" ;
}
else if ( $context == "test" ) {
	define( "COMMON_DIRECTORY" , "../../dev_htdocs/live/common/" ) ;
	$debug = false ;

	$db_username = "developers" ;
	$db_password = "gAt01at390" ;
	$db_database = "test_logan" ;
	$db_host = "10.20.26.29" ;

	$checkpoint_service = "dev" ;
	$checkpoint_application_name = "dev" ;
	$checkpoint_application_password = "dev" ;
	$checkpoint_context = "dev" ;
}
else if ( $context == "prod" ) {
	define( "COMMON_DIRECTORY" , "/srv/www/htdocs/live/common/" ) ;
	$debug = false ;

	$db_username = "" ;
	$db_password = "" ;
	$db_database = "" ;
	$db_host = "" ;

	$checkpoint_service = "dev" ;
	$checkpoint_application_name = "dev" ;
	$checkpoint_application_password = "dev" ;
	$checkpoint_context = "dev" ;
}

$shutdown_status = 503 ;
$shutdown_interval = 5 ;
$shutdown_url = "" ;

define( "SPIFFY_MODEL_VERSION" , "2.1" ) ;
define( "BOOTSTRAP_VERSION" , 2.1 ) ;
define( "SYSTEM_USER_ID" , 1 ) ;
define( "RESULTS_PER_PAGE" , 50 ) ;
define( "COMMON_APP_CLASSES_DIRECTORY" , COMMON_DIRECTORY . "classes/dm/" ) ;

?>
