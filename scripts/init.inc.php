<?
/**
 * Testes unit�rios para classes comuns.
 *
 * @package		spiffy-framework
 * @subpackage	tests
 * @author		rthiago
 * @since		2013-02-04 18:36
 */

require( "./scripts/context.inc.php" ) ;
require( "./scripts/config.inc.php" ) ;

// Classes do Spiffy
require( COMMON_DIRECTORY . "classes/spiffy_util.inc.php" ) ;

// Classes comuns
require( "./scripts/util.inc.php" ) ;

// Configura��o da aplica��o
$application_name = "tests" ;
$application_version = "1.0" ;
$application_release_date = "20130204" ;
$application_description = "Tests" ;
$application_module = "tests" ;

// Configura��o para tratamento de eventos
$event = new spiffy_event( array() , $application_name , $application_version , $application_release_date ) ;
$event->set_debug( $debug ) ;
$event->set_shutdown_config( $shutdown_url , $shutdown_status ) ;
$event->prepare() ;
$event->check_environment() ;

// Configura��o para autentica��o do checkpoint
$checkpoint = new spiffy_checkpoint( $checkpoint_application_name , $checkpoint_application_password , $checkpoint_service , $checkpoint_context ) ;
$checkpoint->set_event_handler( array( "instance" => $event , "method" => "add" ) ) ;


// Configura��o da conex�o com o banco de dados
$connection = new spiffy_db_connection() ;

$connection->open( $db_username , $db_password , $db_database , $db_host ) ;

?>