<?
/**
 * Testes unit�rios para classes comuns.
 *
 * @package		spiffy-framework
 * @subpackage	tests
 * @author		rthiago
 * @since		2013-02-04 18:34
 */

/**
 * Tenta carregar automaticamente arquivos de classes.
 *
 * @param	string	$class_name	Nome da classe.
 *
 * @return	void
 * @author	acarmona
 * @author	Wagner Andreoli <wagner.andreoli@grupofolha.com.br>
 * @since	2011-12-26 16:29:59
 */
function autoload( $class_name ) {
	if ( ! class_exists( $class_name ) ) {
		if ( strpos( $class_name , "_test" ) !== false ) {
			require_once( COMMON_APP_CLASSES_DIRECTORY . $class_name . ".class.php" ) ;
		}
		else if ( strpos( $class_name , "spiffy" ) !== false ) {
			require_once( COMMON_DIRECTORY . "classes/" . $class_name . ".class.php" ) ;
		}
		else if ( strpos( $class_name , "permission" ) === 0 ) {
			require_once( COMMON_DIRECTORY . "classes/permission/" . $class_name . ".class.php" ) ;
		}
		else if ( strpos( $class_name , "_controller" ) !== false ) {
			require_once( SCRIPTS_DIRECTORY . $class_name . ".class.php" ) ;
		}
		else if ( preg_match( "/^PHP(Unit|_Invoker)/" , $class_name ) ) {
			// O PHPUnit usa class_exists que faz com que este autoload seja executado.
			// Ao cair no else abaixo, e a classe n�o existir vai dar fatal error.
			// Ent�o se for qualquer coisa come�ando com PHPUnit* ou PHP_Invoker*, vou
			// ignorar e deixar para o autoload do PHPUnit tratar.
			return ;
		}
		else {
			require_once(  COMMON_DIRECTORY .'/classes/dm/'. $class_name . ".class.php" ) ;
		}
	}

	// Se certifica de que vari�veis declaradas dentro dos arquivos de classes sejam globais.
	// � necess�rio devido a algumas classes antigas terem vari�veis globais de controle dentro do
	// arquivos da classe. (Exemplo: $default_critical_behaviour em spiffy_event.class.php).
	foreach ( get_defined_vars() as $key => $value ) {
		// Se j� est� definido no escopo global, ent�o n�o sobrescreve
		if ( ! isset( $GLOBALS{$key} ) ) {
			$GLOBALS{$key} = $value ;
		}
	}
}

spl_autoload_register( "autoload" ) ;

?>