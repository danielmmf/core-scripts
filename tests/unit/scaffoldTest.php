<?php

require "../scaffold.php"; 

class scaffoldTest extends PHPUnit_Framework_TestCase{
    private $scaffold;
    
    public function setUp(){
        $this->scaffold = new Scaffold;
    }
    
    public function testShouldReceiveModelName(){
        $model_name = "usuario";
        $expected = $model_name;
        //$this->assertEquals($expected, $this->scaffold->create($model_name));
        $this->scaffold->create($model_name);
    }
    
    public function testShouldReturnModelName(){
        $model_name = "usuario";
        $expected = $model_name;
        $response = $this->scaffold->create($model_name);
        $this->assertEquals($expected, $response['model']);
    }
    
    public function testShoulReturnStatus(){
        $expected = "OK";
        $model_name = "usuario";
        $response = $this->scaffold->create($model_name);
        $this->assertEquals($expected, $response['status']);
    }
    
}

?>
